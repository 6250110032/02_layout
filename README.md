import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Layout',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'First Layout'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        color: Colors.lightBlueAccent[100],
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
        child: Column(
          children: [
            Icon(
              Icons.insert_photo,
            size: 200,
            ),
             Row(
               mainAxisAlignment: MainAxisAlignment.spaceAround,
               children: const [
                 Text(
                   'Row Chile 1',
                 style: TextStyle(
                     fontSize: 18,
                     fontWeight: FontWeight.bold,
                 ),
                 ),
                 Text(
                   'Row Chile 2',
                   style: TextStyle(
                       fontSize: 18,
                       fontWeight: FontWeight.bold,
                   ),
                 ),
                 Text(
                   'Row Chile 3',
                   style: TextStyle(
                       fontSize: 18,
                       fontWeight: FontWeight.bold,
                   ),
                 ),
               ],
             ),
            Padding(
                padding: const EdgeInsets.all(30),
            child: Text('This is column',
            style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
            ),
            ),

             Padding(
               padding: const EdgeInsets.all(15.0),
               child: TextField(
                 decoration: InputDecoration(
                   hintText: 'Username',
                   fillColor: Colors.white,
                   filled: true,
                 ),
               ),
             ),
            Padding(
              padding: const EdgeInsets.only(left: 15,right: 15, bottom:  30),
              child: TextField(
                decoration: InputDecoration(
                  hintText: 'Password',
                  fillColor: Colors.white,
                  filled: true,
                ),
              ),
            ),
            // Row(),
          ],
        ),
        ),
        ),
      );

  }
}